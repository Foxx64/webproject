package FileWebProject.Interfaces;
import FileWebProject.Entitys.Computer;
import FileWebProject.Exceptions.EntityNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public interface ComputerServiceInterface {
    ArrayList<Computer> getAll();
    void addComputer(Computer computer);
    Computer getComputerById(Integer id)  throws EntityNotFoundException;
    void remove(Computer computer);
    void updateComputer(Computer computer);
}
