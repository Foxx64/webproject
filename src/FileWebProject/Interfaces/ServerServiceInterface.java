package FileWebProject.Interfaces;

import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;

public interface ServerServiceInterface {
    ArrayList<Server> getAll() throws IOException, EntityNotFoundException;
    void addServer(Server server) throws IOException, EntityNotFoundException;
    Server getServerById(Integer id) throws EntityNotFoundException, IOException;
    void remove(Server server);
    void removeComputer(Server server, Integer computersId) throws EntityNotFoundException;
    void addComputer(Server server, Computer computer) throws EntityNotFoundException ;
    void updateServer(Server server);
}