package FileWebProject.Services;

import FileWebProject.ComputersEntity;
import FileWebProject.Entitys.Computer;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import java.util.ArrayList;

public class HibernateComputerService implements ComputerServiceInterface{
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            configuration.setProperty("hibernate.connection.username", "c##nata");
            configuration.setProperty("hibernate.connection.password", "123");

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    @Override
    public ArrayList<Computer> getAll() {
        ArrayList<Computer> computers = new ArrayList<>();
        final Session session = getSession();
        try {
            final Query query = session.createQuery("from ComputersEntity");
            for (Object object : query.list()) {
                ComputersEntity entity = (ComputersEntity) object;
                Computer computer = new Computer();
                computer.setName(entity.getName());
                computer.setOsVersion(entity.getOsversion());
                computer.setStorage(entity.getStorage());
                computer.setIp(entity.getIp());
                computer.setId(entity.getId());
                computers.add(computer);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return computers;
    }

    @Override
    public void addComputer(Computer computer) {
        final Session session = getSession();
        ComputersEntity entity = new ComputersEntity();
        entity.setName(computer.getName());
        entity.setOsversion(computer.getOsVersion());
        entity.setIp(computer.getIp());
        entity.setStorage(computer.getStorage());
        entity.setId(computer.getId());
        try {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Computer getComputerById(Integer id) throws EntityNotFoundException {
        final Session session = getSession();
        ComputersEntity entity;
        Computer computer = null;
        try {
            entity = (ComputersEntity) session.load(ComputersEntity.class, id);
            computer = new Computer();
            computer.setName(entity.getName());
            computer.setOsVersion(entity.getOsversion());
            computer.setStorage(entity.getStorage());
            computer.setIp(entity.getIp());
            computer.setId(entity.getId());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
        computer.getId();
        final Session session = getSession();
        try {
            ComputersEntity entity = new ComputersEntity();
            session.beginTransaction();
            entity.setId(computer.getId());
            session.delete(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updateComputer(Computer computer) {
        final Session session = getSession();
        try {
            ComputersEntity computersEntity = (ComputersEntity) session.load(ComputersEntity.class, computer.getId());
            computersEntity.setName(computer.getName());
            computersEntity.setOsversion(computer.getOsVersion());
            computersEntity.setIp(computer.getIp());
            computersEntity.setStorage(computer.getStorage());
            computersEntity.setId(computer.getId());
            session.beginTransaction();
            session.update(computersEntity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
