package FileWebProject.Services;


import FileWebProject.ComputersEntity;
import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.ServersEntity;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.io.IOException;
import java.util.ArrayList;

public class HibernateServerService implements ServerServiceInterface {
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            configuration.setProperty("hibernate.connection.username", "c##nata");
            configuration.setProperty("hibernate.connection.password", "123");

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    @Override
    public ArrayList<Server> getAll() throws IOException, EntityNotFoundException {
        final Session session = getSession();
        ArrayList<Server> servers = new ArrayList<Server>();
        try {
            final Query query = session.createQuery("from ServersEntity");
            for (Object object : query.list()) {
                ServersEntity entity = (ServersEntity) object;
                Server server = new Server();
                server.setName(entity.getName());
                server.setOsVersion(entity.getOsversion());
                server.setStorage(entity.getStorage());
                server.setIp(entity.getIp());
                server.setId(entity.getId());
                servers.add(server);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return servers;
    }

    @Override
    public void addServer(Server server) throws IOException, EntityNotFoundException {
        final Session session = getSession();
        ServersEntity entity = new ServersEntity();
        entity.setName(server.getName());
        entity.setOsversion(server.getOsVersion());
        entity.setIp(server.getIp());
        entity.setStorage(server.getStorage());
        entity.setId(server.getId());
        try {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Server getServerById(Integer id) throws EntityNotFoundException, IOException {
        final Session session = getSession();
        ServersEntity entity;
        Server server = null;
        try {
            entity = (ServersEntity) session.load(ServersEntity.class, id);
            server = new Server();
            server.setName(entity.getName());
            server.setOsVersion(entity.getOsversion());
            server.setStorage(entity.getStorage());
            server.setIp(entity.getIp());
            server.setId(entity.getId());
            final Query query = session.createQuery("from ComputersEntity where idservers =" +  server.getId());
            for (Object object : query.list()) {
                ComputersEntity computerEntity = (ComputersEntity) object;
                Computer computer = new Computer();
                computer.setName(computerEntity.getName());
                computer.setOsVersion(computerEntity.getOsversion());
                computer.setStorage(computerEntity.getStorage());
                computer.setIp(computerEntity.getIp());
                computer.setId(computerEntity.getId());
                server.serverComputers.add(computer);
            }
            } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return server;
    }

    @Override
    public void remove(Server server) {
        server.getId();
        final Session session = getSession();
        try {
            ServersEntity entity = new ServersEntity();
            session.beginTransaction();
            entity.setId(server.getId());
            session.delete(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void removeComputer(Server server, Integer computersId) throws EntityNotFoundException {
        final Session session = getSession();
        ComputerServiceInterface service = new HibernateComputerService();
        Computer computer = service.getComputerById(computersId);
        ComputersEntity entity =(ComputersEntity) session.load(ComputersEntity.class, computer.getId());
        entity.setId(computer.getId());
        entity.setIdservers(null);
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void addComputer(Server server, Computer computer) throws EntityNotFoundException {
        final Session session = getSession();
        ComputersEntity entity =(ComputersEntity) session.load(ComputersEntity.class, computer.getId());
        entity.setId(computer.getId());
        entity.setIdservers(server.getId());
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updateServer(Server server) {
        final Session session = getSession();
        ServersEntity entity = new ServersEntity();
        entity.setName(server.getName());
        entity.setOsversion(server.getOsVersion());
        entity.setIp(server.getIp());
        entity.setStorage(server.getStorage());
        entity.setId(server.getId());
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
