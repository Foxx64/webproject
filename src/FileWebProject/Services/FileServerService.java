package FileWebProject.Services;


import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.Servlets.WebProject;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileServerService implements ServerServiceInterface {

    @Override
    public ArrayList<Server> getAll() throws IOException, EntityNotFoundException {
        File dir = new File(getServerPath());
        File[] files = dir.listFiles();
        ArrayList<Server>servers = new ArrayList<Server>();
        assert files != null;
        for (File file : files) {
            if (file.isFile()) {
                try {
                    String fileName = file.getAbsolutePath();
                    Path path = Paths.get(fileName);
                    byte[] bytes = java.nio.file.Files.readAllBytes(path);
                    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
                    ObjectInputStream ois = new ObjectInputStream(is);
                    Object serverObject = ois.readObject();
                    Server server = (Server) serverObject;
                    servers.add(server);
                } catch (Exception e ) {
                    e.printStackTrace();
                }
            }
        }
        return servers;
     }

    @Override
    public void addServer(Server server) throws IOException, EntityNotFoundException {
        ArrayList<Server>servers = getAll();
        int id = getId(servers);
        server.setId(id);
        updateServer(server);
    }

    @Override
    public Server getServerById(Integer id) throws EntityNotFoundException, IOException {
        ArrayList<Server> servers = getAll();
        Server server = null;
        for (Server server1 : servers) {
            if (server1.getId().equals(id)) {
                server = server1;
            }
        }
        if (server == null)
        {
            throw new EntityNotFoundException();
        }
        return server;
    }

    @Override
    public void remove(Server server) {
        File serverPath = new File (getFileServerPath() + "server_" + server.getId() +".txt");
        serverPath.delete();
    }

    private String getFileServerPath() {
        URL location = WebProject.class.getProtectionDomain().getCodeSource().getLocation();
        String path = null;
        try {
            File file = new File(location.toURI());
            path = file.getPath();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return path + "\\..\\..\\..\\..\\..\\..\\..\\..\\Servers\\";
    }

    @Override
    public void removeComputer(Server server, Integer computersId) throws EntityNotFoundException {
        server.removeServerComputers(computersId);
    }

    @Override
    public void addComputer(Server server, Computer computer) throws EntityNotFoundException {
        server.addServerComputers(computer);
        updateServer(server);
    }

    @Override
    public void updateServer(Server server) {
        String writeServer = getServerPath()+ "server_" + server.getId() +".txt";
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(server);
            byte[] bytes = os.toByteArray();
            java.nio.file.Files.write(Paths.get(writeServer), bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    private String getServerPath() {
        return "C:\\Sources\\WebProject\\Servers\\";
    }

    public int getId(ArrayList<Server> servers ) {

        if (servers.size() == 0) {
            return 0;
        } else {
           if (servers.get(0).getId() == null){
               servers.get(0).setId(0);
               return 0;
           } else {
               int max = servers.get(0).getId();
               for (int i = 1; i < servers.size(); i++) {
                   int itemId = servers.get(i).getId();
                   if (max < itemId) {
                       max = itemId;
                   }
               }
               return max + 1;
           }
        }
    }
 }
