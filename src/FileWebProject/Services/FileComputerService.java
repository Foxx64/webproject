package FileWebProject.Services;


import FileWebProject.Entitys.Computer;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Servlets.WebProject;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileComputerService implements ComputerServiceInterface {




    @Override
    public ArrayList<Computer> getAll() {
        File dir = new File(getComputerPath());
        File[] files = dir.listFiles();
        assert files != null;
        ArrayList<Computer> computers = new ArrayList<Computer>();
        for (File file : files) {
            if (file.isFile()) {
                try {
                    String fileName = file.getAbsolutePath();
                    Path path = Paths.get(fileName);
                    byte[] bytes = java.nio.file.Files.readAllBytes(path);
                    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
                    ObjectInputStream ois = new ObjectInputStream(is);
                    Object computerObject = ois.readObject();
                    Computer computer = (Computer) computerObject;
                    computers.add(computer);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return computers;
    }

    @Override
    public void addComputer(Computer computer) {
        ArrayList <Computer> computers = getAll();
        int id = getId(computers);
        computer.setId(id);
        updateComputer(computer);
    }

    public int getId(ArrayList<Computer> computers) {
        if (computers.size() == 0) {
            return 0;
        } else {
            if(computers.get(0).getId()== null){
                computers.get(0).setId(0);
                return 0;
            } else {
                int max = computers.get(0).getId();
                for (int i = 1; i < computers.size(); i++) {
                    int itemId = computers.get(i).getId();
                    if (max < itemId) {
                        max = itemId;
                    }
                }
                return max + 1;
            }
        }
    }

    @Override
    public Computer getComputerById(Integer id) throws EntityNotFoundException {
        ArrayList<Computer> computers = getAll();
        Computer computer = null;
        for (Computer computer1 : computers) {
            if (computer1.getId().equals(id)) {
                computer = computer1;
            }
        }
        if (computer == null) {
            throw new EntityNotFoundException();
        }
        return computer;
    }

    @Override
    public void remove(Computer computer) {
        File computerPath = new File (getFileComputerPath() + "computer_" + computer.getId() +".txt");
        computerPath.delete();
    }

    private String getFileComputerPath() {
        URL location = WebProject.class.getProtectionDomain().getCodeSource().getLocation();
        String path = null;
        try {
            File file = new File(location.toURI());
            path = file.getPath();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return path + "\\..\\..\\..\\..\\..\\..\\..\\..\\Computers\\";
    }

    @Override
    public void updateComputer(Computer computer) {
        String writeComputer = getComputerPath()+"computer_" + computer.getId() + ".txt";
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(computer);
            byte[] bytes = os.toByteArray();
            java.nio.file.Files.write(Paths.get(writeComputer), bytes);
        } catch (IOException e) {
        }
    }

    public String getComputerPath(){
        return "C:\\Sources\\WebProject\\Computers\\";
    }
}