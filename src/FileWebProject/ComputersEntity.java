package FileWebProject;

import javax.persistence.*;

@Entity
@Table(name = "COMPUTERS", schema = "C##NATA", catalog = "")
public class ComputersEntity {
    private String osversion;
    private String name;
    private String storage;
    private String ip;
    private Integer id;
    private Integer idservers;

    @Basic
    @Column(name = "OSVERSION", nullable = true, insertable = true, updatable = true, length = 20)
    public String getOsversion() {
        return   osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    @Basic
    @Column(name = "NAME", nullable = true, insertable = true, updatable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "STORAGE", nullable = true, insertable = true, updatable = true, length = 20)
    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    @Basic
    @Column(name = "IP", nullable = true, insertable = true, updatable = true, length = 20)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Id
    @Column(name = "ID", nullable = false, insertable = true, updatable = true, precision = 0)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "IDSERVERS", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getIdservers() {
        return idservers;
    }

    public void setIdservers(Integer idservers) {
        this.idservers = idservers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComputersEntity that = (ComputersEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (idservers != null ? !idservers.equals(that.idservers) : that.idservers != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (osversion != null ? !osversion.equals(that.osversion) : that.osversion != null) return false;
        if (storage != null ? !storage.equals(that.storage) : that.storage != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = osversion != null ? osversion.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (storage != null ? storage.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (idservers != null ? idservers.hashCode() : 0);
        return result;
    }
}
