package FileWebProject.JavaBeans;

import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.Services.HibernateComputerService;
import FileWebProject.Services.HibernateServerService;
import FileWebProject.Servlets.WebProject;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;


@Stateless(name = "ServerSessionEJB")
public class ServerSessionBean {

    public ArrayList<Server> getServers() throws IOException, EntityNotFoundException {
        ServerServiceInterface service = WebProject.getServerService();
        ArrayList<Server> servers = service.getAll();
        return servers;
    }

    public ArrayList<Server> getFilteredServerList(String searchString) throws IOException, EntityNotFoundException {
        ArrayList<Server> servers = getServers();
        if (searchString == null) {
            return servers;
        } else {
            ArrayList<Server> searchServer = computerFilter(servers, searchString);
            return searchServer;
        }
    }

    public ArrayList<Server> computerFilter(ArrayList<Server> servers, String searchString) {
        ArrayList<Server> searchServer = new ArrayList<Server>();
        for (Server server : servers) {
            if (server.getName().contains(searchString)) {
                searchServer.add(server);
            }
        }
        return searchServer;
    }

    public void addServer(HttpServletRequest request) throws IOException, EntityNotFoundException {
        ServerServiceInterface service = WebProject.getServerService();
        Server server = new Server();
        fill(request, server);
        service.addServer(server);
    }

    public void addServersComputer(String searchComputer, String searchServer) throws IOException {
        ServerServiceInterface service = WebProject.getServerService();
        ComputerServiceInterface serviceComputer = WebProject.getComputerService();
        Integer idServer = Integer.parseInt(searchServer);
        Integer idComputer = Integer.parseInt(searchComputer);
        try {
            Server server = service.getServerById(idServer);
            Computer computer = serviceComputer.getComputerById(idComputer);
            service.addComputer(server, computer);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void deleteServerComputer(String searchComputer, String searchServer) {
        ServerServiceInterface service = WebProject.getServerService();
        Integer serversId = Integer.parseInt(searchServer);
        Integer computersId = Integer.parseInt(searchComputer);
        try {
            Server server = service.getServerById(serversId);
            service.removeComputer(server, computersId);
            service.updateServer(server);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteServer(String search) {
        ServerServiceInterface service = WebProject.getServerService();
        Integer id = Integer.parseInt(search);
        try {
            Server server = service.getServerById(id);
            service.remove(server);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateServer(String search, HttpServletRequest request) {
        ServerServiceInterface service = WebProject.getServerService();
        Integer id = Integer.parseInt(search);
        try {
            Server server = service.getServerById(id);
            fill(request, server);
            service.updateServer(server);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server getServerById(String search) {
        ServerServiceInterface service = WebProject.getServerService();
        Integer id = Integer.parseInt(search);
        Server server = null;
        try {
            server = service.getServerById(id);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return server;
    }

    public void fill(HttpServletRequest request, Server server) {
        String name = request.getParameter("name");
        server.setName(name);
        String osVersion = request.getParameter("osVersion");
        server.setOsVersion(osVersion);
        String storage = request.getParameter("storage");
        server.setStorage(storage);
        String ip = request.getParameter("ip");
        server.setIp(ip);
    }
}
