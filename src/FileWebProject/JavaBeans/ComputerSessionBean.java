package FileWebProject.JavaBeans;

import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.Services.FileComputerService;
import FileWebProject.Services.FileServerService;
import FileWebProject.Servlets.WebProject;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;


@Stateless(name = "ComputerEJB")
public class ComputerSessionBean {
    public ComputerSessionBean() {
    }

    public ArrayList<Computer> getComputers() {
        ComputerServiceInterface service = WebProject.getComputerService();
        return service.getAll();
    }

    public ArrayList<Computer> getFilteredComputerList(String searchString) {
        ArrayList<Computer> computers = getComputers();
        if (searchString == null) {
            return computers;
        } else {
            ArrayList<Computer> searchComputers = computerFilter(computers, searchString);
            return searchComputers;
        }
    }

    public ArrayList<Computer> computerFilter(ArrayList<Computer> computers, String searchString) {
        ArrayList<Computer> searchComputers = new ArrayList<Computer>();
        for (Computer computer : computers) {
            if (computer.getName().contains(searchString)) {
                searchComputers.add(computer);
            }
        }
        return searchComputers;
    }

    public void addComputer(HttpServletRequest request) {
        ComputerServiceInterface service = WebProject.getComputerService();
        Computer computer = new Computer();
        fill(request, computer);
        service.addComputer(computer);
    }

    public ArrayList<Computer> chooseComputers(Server server) {
        ArrayList<Computer> computers = getComputers();
        ArrayList<Computer> serverComputers = new ArrayList<>();
        for (int i = 0; i < computers.size(); i++) {
            if (!server.contains(computers.get(i).getId())) {
                serverComputers.add(computers.get(i));
            }
        }
        return serverComputers;
    }

    public Server getServer(String search) throws IOException, EntityNotFoundException {
        Integer id = Integer.parseInt(search);
        ServerServiceInterface service = WebProject.getServerService();
        Server server = service.getServerById(id);
        return server;
    }

    public void deleteComputer(HttpServletRequest request) {
        ComputerServiceInterface service = WebProject.getComputerService();
        String search = request.getParameter("computerId");
        Integer id = Integer.parseInt(search);
        try {
            Computer computer = service.getComputerById(id);
            service.remove(computer);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateComputer(HttpServletRequest request, String search) {
        ComputerServiceInterface service = WebProject.getComputerService();
        Integer id = Integer.parseInt(search);
        try {
            Computer computer = service.getComputerById(id);
            fill(request, computer);
            service.updateComputer(computer);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Computer getComputerById(String search) {
        ComputerServiceInterface service = WebProject.getComputerService();
        Computer computer = null;
        try {

            Integer id = Integer.parseInt(search);
            computer = service.getComputerById(id);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return computer;
    }

    public void fill(HttpServletRequest request, Computer computer) {
        String name = request.getParameter("name");
        computer.setName(name);
        String osVersion = request.getParameter("osVersion");
        computer.setOsVersion(osVersion);
        String storage = request.getParameter("storage");
        computer.setStorage(storage);
        String ip = request.getParameter("ip");
        computer.setIp(ip);

    }
}

