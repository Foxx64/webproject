package FileWebProject.Servlets;

import FileWebProject.Entitys.Computer;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.JavaBeans.ComputerSessionBean;
import FileWebProject.Services.FileComputerService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet
public class DeleteComputerServlet extends HttpServlet {
    @EJB
    private ComputerSessionBean computerSessionBean;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        computerSessionBean.deleteComputer(request);
            response.sendRedirect("ComputerList");
    }
}
