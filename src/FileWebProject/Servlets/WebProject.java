package FileWebProject.Servlets;

import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.Services.FileComputerService;
import FileWebProject.Services.HibernateComputerService;
import FileWebProject.Services.HibernateServerService;

import javax.servlet.http.HttpServlet;

public class WebProject extends HttpServlet {

    public static ComputerServiceInterface getComputerService() {//метод пока не нужен, но будет нужен когда я буду использовать JPA

        ComputerServiceInterface hibernateComputer = new HibernateComputerService();
        return hibernateComputer;
    }

   public static ServerServiceInterface getServerService() {
       ServerServiceInterface hibernateServer = new HibernateServerService();
       return hibernateServer;
   }
}
