package FileWebProject.Servlets;

import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.JavaBeans.ServerSessionBean;
import FileWebProject.Services.FileServerService;
import FileWebProject.Entitys.Server;
import FileWebProject.Interfaces.ServerServiceInterface;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet
public class DeleteServerServlet extends HttpServlet {
    @EJB
    private ServerSessionBean serverSessionBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("serverId");
        serverSessionBean.deleteServer(search);
        response.sendRedirect("ServerList");
    }
}
