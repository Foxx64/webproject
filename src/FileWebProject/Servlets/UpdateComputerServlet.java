package FileWebProject.Servlets;

import FileWebProject.Entitys.Computer;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.JavaBeans.ComputerSessionBean;
import FileWebProject.Services.FileComputerService;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet
public class UpdateComputerServlet extends HttpServlet {
    @EJB
    private ComputerSessionBean computerSessionBean;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("computerId");
        computerSessionBean.updateComputer(request, search);
        response.sendRedirect("ComputerList");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String search = request.getParameter("computerId");
        Computer computer = computerSessionBean.getComputerById(search);
        session.setAttribute("computer", computer);
        getServletContext().getRequestDispatcher("/updateComputer.jsp").forward(request, response);
    }
}
