package FileWebProject.Servlets;

import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.JavaBeans.ComputerSessionBean;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet
public class ChooseComputerServlet extends HttpServlet {
    @EJB
    private ComputerSessionBean computerSessionBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String search = request.getParameter("serverId");
        try {
            Server server = computerSessionBean.getServer(search);
            ArrayList<Computer> serverComputers = computerSessionBean.chooseComputers(server);
            session.setAttribute("server", server);
            session.setAttribute("serverComputers", serverComputers);
            getServletContext().getRequestDispatcher("/chooseComputer.jsp").forward(request, response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }
}

