package FileWebProject.Servlets;

import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.JavaBeans.ServerSessionBean;
import FileWebProject.Services.FileServerService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet
public class DeleteServerComputerServlet extends HttpServlet {
    @EJB
    private ServerSessionBean serverSessionBean;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchComputer = request.getParameter("computerId");
        String searchServer = request.getParameter("serverId");
        serverSessionBean.deleteServerComputer(searchComputer,searchServer);
        response.sendRedirect("ServerList");
    }
}


