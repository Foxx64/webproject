package FileWebProject.Servlets;

import FileWebProject.Entitys.Computer;
import FileWebProject.JavaBeans.ComputerSessionBean;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


public class ComputerListServlet extends HttpServlet {
    @EJB
    private ComputerSessionBean computerSessionBean;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String searchString = request.getParameter("getParametr");
        ArrayList<Computer> computers = computerSessionBean.getFilteredComputerList(searchString);
        session.setAttribute("computers", computers);
        getServletContext().getRequestDispatcher("/computerList.jsp").forward(request, response);
    }
}