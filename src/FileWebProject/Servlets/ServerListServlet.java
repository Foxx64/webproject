package FileWebProject.Servlets;

import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.JavaBeans.ServerSessionBean;
import FileWebProject.Services.FileServerService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
public class ServerListServlet extends HttpServlet {
    @EJB
    private ServerSessionBean serverSessionBean;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchString = request.getParameter("server");
        HttpSession session = request.getSession();
        try {
            ArrayList<Server> servers = serverSessionBean.getFilteredServerList(searchString);
            session.setAttribute("servers", servers);
            getServletContext().getRequestDispatcher("/serverList.jsp").forward(request, response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
