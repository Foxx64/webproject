package FileWebProject.Servlets;

import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.JavaBeans.ServerSessionBean;
import FileWebProject.Services.FileServerService;
import FileWebProject.Entitys.Server;
import FileWebProject.Interfaces.ServerServiceInterface;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "Servlet")
public class AddServerServlet extends HttpServlet {
    @EJB
    private ServerSessionBean serverSessionBean;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            serverSessionBean.addServer(request);
            response.sendRedirect("ServerList");
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            ArrayList<Server> servers = serverSessionBean.getServers();
            session.setAttribute("servers", servers);
            getServletContext().getRequestDispatcher("/addServer.jsp").forward(request, response);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
