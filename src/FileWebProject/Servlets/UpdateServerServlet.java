package FileWebProject.Servlets;

import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.JavaBeans.ServerSessionBean;
import FileWebProject.Services.FileServerService;
import FileWebProject.Entitys.Server;
import FileWebProject.Interfaces.ServerServiceInterface;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet
public class UpdateServerServlet extends HttpServlet {
    @EJB
    private ServerSessionBean serverSessionBean;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("serverId");
        serverSessionBean.updateServer(search, request);
        response.sendRedirect("ServerList");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String search = request.getParameter("serverId");
        Server server = serverSessionBean.getServerById(search);
        session.setAttribute("server", server);
        getServletContext().getRequestDispatcher("/updateServer.jsp").forward(request, response);
    }
}

