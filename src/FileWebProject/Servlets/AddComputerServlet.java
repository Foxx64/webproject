package FileWebProject.Servlets;

import FileWebProject.Entitys.Server;
import FileWebProject.Entitys.Computer;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.JavaBeans.ComputerSessionBean;
import FileWebProject.Services.FileComputerService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet
public class AddComputerServlet extends HttpServlet {
    @EJB
    private ComputerSessionBean computerSessionBean;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        computerSessionBean.addComputer(request);
        response.sendRedirect("ComputerList");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<Computer> computers = computerSessionBean.getComputers();
        session.setAttribute("computers", computers);
        getServletContext().getRequestDispatcher("/addComputer.jsp").forward(request, response);
    }
}
