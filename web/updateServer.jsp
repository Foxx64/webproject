<%@ page import="java.util.ArrayList" %>
<%@ page import="FileWebProject.Entitys.Server" %>
<%@ page import="FileWebProject.Entitys.Computer" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="./addComputer.css">
  <title>Серверы локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
  <div id="siteName">
    Изменение сервера
  </div>
</div>
<%Server server = (Server)session.getAttribute("server");%>
<form action="UpdateServer" method="post">
  <input type="hidden" name="serverId" value= <%=server.getId()%>><div id="addImg">
  <button>
    <div> Сохранить </div>
  </button>
</div>
  <div class="title">Введите имя сервера:</div>
  <div class="titleText"></id>
    <input type = "text" name="name" value="<%=server.getName()%>">
    </input>
  </div>
  <div class="titleText">Введите версию операционной системы:</div>
  <div class="titleText">
    <input type = "text" name="osVersion"value="<%=server.getOsVersion()%>">
    </input>
  </div>
  <div class="title"> Введите параметры жесткого диска:</div>
  <div class="titleText" id >
    <input type = "text" name="storage" value="<%=server.getStorage()%>">
    </input>
  </div>
  <div class="titleText">Введите IP адрес:</div>
  <div class="titleText">
    <input type = "text" name="ip"value="<%=server.getIp()%>">
    </input>
  </div>
  </div>
</form>
<table border="1">
  <tr>
    <th>Id компьтера</th>
    <th>Имя компьтера</th>
    <th>Параметры жеского диска</th>
    <th>Версия ОС</th>
    <th>IP адрес сервера</th>
    <th></th>
  </tr>
  <% for(Computer computer : server.serverComputers){ %>
  <tr>
    <td><%=computer.getId()%></td>
    <td><%=computer.getName()%></td>
    <td><%=computer.getStorage()%></td>
    <td><%=computer.getOsVersion()%></td>
    <td><%=computer.getIp() %></td>
    <td>
      <form action="DeleteSC" method="post">
        <div>
          <input type="hidden" name="serverId" value="<%=server.getId()%>">
          <input type="hidden" name="computerId" value="<%=computer.getId()%>">
          <button value="Удалить">Удалить связь с сервером</button>
        </div>
      </form>
    </td>
  </tr>
  <% } %>
</table>
</body>
</html>
