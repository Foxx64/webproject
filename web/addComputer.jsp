<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="./addComputer.css">
    <title>Компьютеры локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
    <div id="siteName">
        Новый компьютер
    </div>
</div>
<form action="AddComputer" method="post">
    <div id="addImg">
        <button>
            <div> Сохранить </div>
        </button>
    </div>
    <div class="title">Введите имя компьютера:</div>
    <div class="titleText" id >
        <textarea rows= "1" cols="40" name="name">
        </textarea>
    </div>
    <div class="titleText">Введите версию операционной системы:</div>
    <div class="titleText">
        <textarea rows= "1" cols="40" name="osVersion">
        </textarea>
    </div>
    <div class="title">Введите параметры жесткого диска:</div>
    <div class="titleText" id >
        <textarea rows= "1" cols="40" name="storage">
        </textarea>
    </div>
    <div class="titleText">Введите IP адрес:</div>
    <div class="titleText">
        <textarea rows= "1" cols="40" name="ip">
        </textarea>
    </div>
</form>
</body>
</html>
