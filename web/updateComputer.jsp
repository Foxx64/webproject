<%@ page import="FileWebProject.Entitys.Computer" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="./addComputer.css">
  <title>Компьютеры локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
  <div id="siteName">
    Изменение компьютера
  </div>
</div>
<%Computer computer = (Computer)session.getAttribute("computer");%>
<form action="UpdateComputer" method="post">
    <input type="hidden" name="computerId" value= <%=computer.getId()%>><div id="addImg">
    <button>
        <div> Сохранить </div>
    </button>
</div>
    <div class="title">Введите имя компьютера:</div>
    <div class="titleText"></id>
        <input type = "text" name="name" value="<%=computer.getName()%>">
        </input>
    </div>
    <div class="titleText">Введите версию операционной системы:</div>
    <div class="titleText">
        <input type = "text" name="osVersion"value="<%=computer.getOsVersion()%>">
        </input>
    </div>
    <div class="title"> Введите параметры жесткого диска:</div>
    <div class="titleText" id >
        <input type = "text" name="storage" value="<%=computer.getStorage()%>">
        </input>
    </div>
    <div class="titleText">Введите IP адрес:</div>
    <div class="titleText">
        <input type = "text" name="ip"value="<%=computer.getIp()%>">
        </input>
    </div>
</form>
</body>
</html>
