<%@ page import="java.util.ArrayList" %>
<%@ page import="FileWebProject.Entitys.Server" %>
<%@ page import="FileWebProject.Entitys.Computer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="./serverList.css">
  <title>Серверы локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
  <div id="siteName">
    Доступные серверу компьютеры  </div>
</div>
<%
  Server server = (Server) session.getAttribute("server");
  ArrayList<Computer> serverComputers = (ArrayList<Computer>) session.getAttribute("serverComputers");%>
  <table border="1">
  <caption>
    <form action="ChooseComputerServlet" method="get">
      <div id="textField">
        <input type="text" name="getParametr">
        <button>
          <img src="./search_btn4n.png" />
        </button>
      </div>
    </form>
  </caption>
  <tr>
    <th>Id компьтера</th>
    <th>Имя компьтера</th>
    <th>Параметры жеского диска</th>
    <th>Версия ОС</th>
    <th>IP адрес сервера</th>
    <th></th>
  </tr>
  <% for(Computer computer : serverComputers){ %>
  <tr>
    <td><%=computer.getId()%></td>
    <td><%=computer.getName()%></td>
    <td><%=computer.getStorage()%></td>
    <td><%=computer.getOsVersion()%></td>
    <td><%=computer.getIp() %></td>
    <td>
      <a class="actionButton" href="AddServerComputer?serverId=<%=server.getId()%>&computerId=<%=computer.getId()%>">Добавить компьютер к серверу</a>
    </td>
  </tr>
    <% } %>
  </table>
</body>
</html>
