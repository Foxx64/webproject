<%@ page import="FileWebProject.Entitys.Server" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="./addServer.css">
  <title>Серверы локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
  <div id="siteName">
    Новый сервер
  </div>
</div>
<form action="AddServer" method="post">
  <div id="addImg">
    <button>
      <div> Сохранить </div>
    </button>
  </div>
  <div class="title">Введите имя сервера:</div>
  <div class="titleText"></id>
    <input type = "text" name="name" >
    </input>
  </div>
  <div class="titleText">Введите версию операционной системы:</div>
  <div class="titleText">
    <input type = "text" name="osVersion">
    </input>
  </div>
  <div class="title"> Введите параметры жесткого диска:</div>
  <div class="titleText" id >
    <input type = "text" name="storage">
    </input>
  </div>
  <div class="titleText">Введите IP адрес:</div>
  <div class="titleText">
    <input type = "text" name="ip">
    </input>
  </div>
  </div>
</form>
</body>
</html>
