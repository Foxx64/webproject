<%@ page import="java.util.ArrayList" %>
<%@ page import="FileWebProject.Entitys.Server" %>
<%@ page import="FileWebProject.Entitys.Computer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="./serverList.css">
    <title>Серверы локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
    <div id="siteName">
        Серверы локальной сети
    </div>
</div>
<%
    ArrayList<Server> servers = (ArrayList<Server>) session.getAttribute("servers");
    if (servers.size() == 0){ %>
<table border="1">
    <tr>
        <th>Id сервера</th>
        <th>Имя сервера</th>
        <th>Параметры жеского диска</th>
        <th>Версия операционной системы</th>
        <th>IP адрес сервера</th>
    </tr>
</table>
<div id="title">Запрос не дал результатов</div>
<% } else { %>
<table border="1">
    <caption>
        <form action="ServerList" method="get">
            <div id="textField">
                <input type="text" name="server">
                <button>
                    <img src="./search_btn4n.png" />
                </button>
            </div>
        </form>
    </caption>
    <tr>
        <th>Id сервера</th>
        <th>Имя сервера</th>
        <th>Параметры жеского диска</th>
        <th>Версия ОС</th>
        <th>IP адрес сервера</th>
        <th></th>
        <th>
            <a id="button" href="AddServer">Добавить сервер</a>
        </th>
    </tr>
        <% for(Server server : servers){ %>
    <tr>
        <td><%= server.getId()%></td>
        <td><%= server.getName()%></td>
        <td><%= server.getStorage()%></td>
        <td><%= server.getOsVersion()%></td>
        <td><%= server.getIp() %></td>
        <td><a id="addComputerButton" href="ChooseComputer?serverId=<%=server.getId()%>">Добавить компьютер</a></td>
        <td>
            <a class="actionButton" href="DeleteServer?serverId=<%=server.getId() %>">Удалить сервер</a>
            <a class="actionButton" href="UpdateServer?serverId=<%=server.getId()%>">Изменить сервер</a>
        </td>
    </tr>
    <% } %>
</table>
<% } %>
</body>
</html>
