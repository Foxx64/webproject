<%@ page import="java.util.ArrayList" %>
<%@ page import="FileWebProject.Entitys.Computer" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="./styleTable.css">
    <title>Компьютеры локальной сети</title>
</head>
<body>
<div id="line"></div>
<div id="headLine">
    <div id="siteName">
        Компьютеры локальной сети
    </div>
</div>
<%
    ArrayList<Computer> computers = (ArrayList<Computer>) session.getAttribute("computers");
    if (computers.size() == 0){ %>
<table border="1">
    <tr>
        <th>Id компьютера</th>
        <th>Имя компьютера</th>
        <th>Параметры жеского диска</th>
        <th>Версия операционной системы</th>
        <th>IP адрес компьютера</th>
    </tr>
</table>
    <div id="title">Запрос не дал результатов</div>
<% } else { %>
<table border="1">
    <caption>
        <form action="ComputerList" method="get">
            <div id="textField">
                <input type="text" name="getParametr">
                <button>
                    <img src="./search_btn4n.png" />
                </button>
            </div>
        </form>
    </caption>
    <tr>
        <th>Id компьютера</th>
        <th>Имя компьютера</th>
        <th>Параметры жеского диска</th>
        <th>Версия операционной системы</th>
        <th>IP адрес компьютера</th>
        <th>
            <a id="button" href="AddComputer">Добавить компьютер</a>
        </th>
    </tr>
    <% for(Computer computer : computers){ %>
    <tr>
        <td><%= computer.getId() %></td>
        <td><%= computer.getName() %></td>
        <td><%= computer.getStorage() %></td>
        <td><%= computer.getOsVersion() %></td>
        <td><%= computer.getIp() %></td>
        <td>
            <a class="actionButton" href="DeleteComputer?computerId=<%=computer.getId() %>">Удалить компьютер</a>
            <a class="actionButton" href="UpdateComputer?computerId=<%=computer.getId()%>">Изменить компьютер</a>
        </td>
    </tr>
    <% } %>
</table>
<% } %>
</body>
</html>
