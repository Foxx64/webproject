package example;
import FileWebProject.Entitys.Computer;
import FileWebProject.Entitys.Server;
import FileWebProject.Exceptions.EntityNotFoundException;
import FileWebProject.Interfaces.ComputerServiceInterface;
import FileWebProject.Interfaces.ServerServiceInterface;
import FileWebProject.Services.HibernateComputerService;
import FileWebProject.Services.HibernateServerService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.util.ArrayList;

@WebService()
public class LocalNetworkWebservice {
  @WebMethod
  public void addComputer(Computer computer) {
    ComputerServiceInterface service = new HibernateComputerService();
    service.addComputer(computer);
  }

  @WebMethod
  public ArrayList<Computer> getAllComputer(){
    ComputerServiceInterface service = new HibernateComputerService();
    ArrayList<Computer>computers = service.getAll();
    return computers;
  }

  public Computer getComputerById(Integer id) throws EntityNotFoundException {
    ComputerServiceInterface service = new HibernateComputerService();
    Computer computer = service.getComputerById(id);
    return computer;
  }

  public void updateComputer(Computer computer){
    ComputerServiceInterface service = new HibernateComputerService();
    service.addComputer(computer);
  }

  public void removeComputer(Computer computer){
    ComputerServiceInterface service = new HibernateComputerService();
      service.remove(computer);
  }


  public ArrayList<Server> getAllServer() throws IOException, EntityNotFoundException {
    ServerServiceInterface service = new HibernateServerService();
    ArrayList<Server> servers = service.getAll();
    return servers;
  }

  public void addServer(Server server) throws IOException, EntityNotFoundException {
    ServerServiceInterface service = new HibernateServerService();
    service.addServer(server);
  }

  public Server getServerById(Integer id) throws EntityNotFoundException, IOException {
    ServerServiceInterface service = new HibernateServerService();
    Server server = service.getServerById(id);
    return server;
  }
  public void removeServer(Server server) {
    ServerServiceInterface service = new HibernateServerService();
    service.remove(server);
  }

  public void removeFromComputer(Server server, Integer computersId) throws EntityNotFoundException {
    ServerServiceInterface service = new HibernateServerService();
    service.removeComputer(server,computersId);
  }

  public void addComputerToServer(Server server, Computer computer) throws EntityNotFoundException {
    ServerServiceInterface service = new HibernateServerService();
    service.addComputer(server, computer);
  }

  public void updateServer(Server server) {
    ServerServiceInterface service = new HibernateServerService();
    service.updateServer(server);
  }

  public static void main(String[] argv) {
    Object implementor = new LocalNetworkWebservice();
    String address = "http://localhost:9000/LocalNetworkWebservice";
    Endpoint.publish(address, implementor);
  }
}
